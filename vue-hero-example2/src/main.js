import Vue from "vue";
import './plugins/vuetify'
import App from "./App.vue";
import Hero from "../../vue-hero/src/index.js";
import router from "./router";

Vue.use(Hero);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
