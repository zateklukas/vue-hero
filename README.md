# vue-hero

A Vue.js project for "Hero" transition based on styles

<p align="center">
  <a href="LICENSE">
    <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square" alt="Software License" />
  </a>
  <a href="https://npmjs.org/package/vue-hero">
    <img src="https://img.shields.io/npm/v/vue-hero.svg?style=flat-square" alt="Packagist" />
  </a>
  <a href="https://gitlab.com/zateklukas/vue-hero/issues">
    <img src="https://img.shields.io/badge/Issues-Gitlab-orange.svg" alt="Issues" />
  </a>
</p>

### Preview

![Example](https://j.gifs.com/L7vW0g.gif)

### Installation

```
npm install --save vue-hero
```

or

```
yarn add vue-hero
```

## Usage

First of all, import vue-hero and use it with Vue

```javascript
import Hero from "vue-hero";
Vue.use(Hero);
```

This registers Hero component globally in your Vue project.
Hero component accepts following properties:

```javascript
props:{
  tag:{
    type:[String,Number] // Unique tag of the component
  },
  styles:{
    type: Array // JS representations of CSS properties, f.e. ["borderRadius"] etc
  },
  duration:{
    type: Number // in miliseconds
  },
  easing:{
    type: String // css transition representation, f.e. "ease" or "cubic-bezier(0,.77,.9,.04)"
  }
}
```

Use this component to wrap the element you wish to use in transition. Mark shared elements with hero-tag attribute, which is the same as vue-tag in your Hero component.

```
hero-deep="true" // copies element with it's children if true
hero-index="1"   // z-index of copied elements
hero-tag="0"     // If you've got multiple Hero components in single component, please use same hero-tag as wrapping tag in Hero component
ref = "unique" // Unique identifier of component, you wish to share
```

### Usage in single component

It can be used with `v-if` and `v-else`, but keep in mind using different `key` in order to get it working. Animation happens in mounted() method, which fires, if the two Hero components have different key, but the same tag.

```vue
<Hero
  :tag="0"
  v-if="isGrid"
  key="grid"
  :styles="['borderRadius', 'fontSize', 'boxShadow']"
>
            <v-layout row wrap>
                <v-flex xs6 style = "padding:15px" v-for = "(item, index) in movies" :key = "index">
                    <div hero-deep="false" :hero-index = "-1" :hero-tag="0" :ref="`card_${index}`" style = "background:#FFF; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);">
                        <div :hero-index = "1" :hero-tag="0" hero-deep="true" :ref = "`image_${index}`" class = "media_image" :style = "`width:100%; height:250px;overflow:hidden`">
                            <img :src = "item.poster" style = "width:100%; height:100%">
                        </div>
                        <v-card-text>
                            <h4 :hero-tag="0" :ref = "`title_${index}`" style = "font-size:12px; overflow:hidden; text-overflow:ellipsis; white-space:nowrap">{{item.title}}</h4>
                        </v-card-text>
                    </div>
                </v-flex>
            </v-layout>
        </Hero>
<Hero :tag="0" key="list" v-else :styles="['borderRadius', 'fontSize']">
            <v-layout column justify-center>
                <template v-for = "(item, index) in movies">
                    <div :hero-index = "-1" hero-deep="false" :key = "index" :hero-tag="0" :ref="`card_${index}`" style = "background:#FFF; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
                        <v-layout align-center row  >
                            <div hero-deep="true" :hero-tag="0" :ref = "`image_${index}`" class = "media_image" :style = "`margin:15px; width:48px; height:48px; border-radius:50%; overflow:hidden`">
                                <img :src = "item.poster" style = "min-width:100%; max-height:100%">
                            </div>
                            <h4 :hero-tag="0" :ref = "`title_${index}`" style = "font-size:14px;">{{item.title}}</h4>
                        </v-layout>
                    </div>
                </template>
            </v-layout>
        </Hero>
```

This result piece of code results in this

![Example](https://j.gifs.com/jZ06Jz.gif)

### Usage with VueRouter

Same goes to usage with VueRouter. Just keep in mind to use the same `ref` for each component you want to share.

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Issues

Feel free to file an Issue if you find a bug at [Issue tracker](https://gitlab.com/zateklukas/vue-hero/issues)

## Credits

- [Lukáš Zátek][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[link-author]: https://github.com/fragileqt
