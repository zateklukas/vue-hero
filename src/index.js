import Hero from "./components/Hero.vue";

export default {
  install(Vue, options) {
    Vue.component("Hero", Hero);
    Vue.prototype.$hero = {
      items: {},
      inUse: {},
    };
  },
};
